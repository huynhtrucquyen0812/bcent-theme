<!DOCTYPE html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<link rel="profile" href="http://gmgp.org/xfn/11" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?> > <!--Thêm class tượng trưng cho mỗi trang lên <body> để tùy biến-->
	<div id="container">
		<header id="header">
			<?php bcent_logo(); ?>
		</header>
		<div class="ribbon">
			<div class="ribbon-stitches-top"></div>
			<strong class="ribbon-content">
				<?php bcent_menu('primary-menu'); ?>
			</strong>
			<div class="ribbon-stitches-bottom"></div>
		</div>
		<div class="clear"></div>