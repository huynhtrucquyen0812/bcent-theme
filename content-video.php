<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
	<header class="entry-header">
		<?php bcent_entry_header(); ?>
	</header>
	<div class="entry-content">
		<?php the_content(); ?>
		<?php (is_single()?bcent_entry_tag() : ''); ?>
	</div>
</article>