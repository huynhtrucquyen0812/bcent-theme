			<footer id="footer">
				<div class="copyright">
					© <?php echo date('Y'); ?> <?php bloginfo( 'sitename' ); ?>. <?php _e('All rights reserved', 'haithien25'); ?>.
				</div>
				<?php
					$copyright = 'Designed by haithien25';
					echo apply_filters( 'bcent_copyright', $copyright );
				?>
			</footer>
		</div> <!--end #container-->
		<?php wp_footer(); ?>
	</body> <!--end body-->
</html> <!-- end html-->