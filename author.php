<?php get_header(); ?>
<div class="content">
	<div class="author-box"><?php
		// Hiển thị avatar tác giả
		echo '<div class="author-avatar">'.get_avatar( get_the_author_meta( 'ID' )).'</div>' ;
		// Hiển thị tên tác giả
		printf('<h3>'.__('Posts by %1$s', 'bcent').'</h3>', get_the_author());
		// Hiển thị giới thiệu của tác giả
		echo '<p>'.get_the_author_meta( 'description' ).'</p>';
		// Hiển thị view website của tác giả
		if(get_the_author_meta( 'user_url' ))
			printf(__('<a href="%1$s" title="Visit %2$s\'s website" target="blank">Visit my website</a>', 'bcent'), get_the_author_meta( 'user_url' ), get_the_author());
	?></div>

	<section id="main-content">
		<?php if(have_posts()) : while(have_posts()) : the_post(); ?>
				<?php get_template_part( 'content', get_post_format() ); ?>
			<?php endwhile; ?>
			<?php bcent_pagination(); ?>
		<?php else : ?>
			<?php get_template_part( 'content', 'none' ); ?>
		<?php endif; ?>
	</section>
	<section id="sidebar">
		<?php get_sidebar(); ?>
	</section>
</div>
<?php get_footer(); ?>