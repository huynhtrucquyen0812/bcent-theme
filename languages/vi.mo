��    !      $  /   ,      �  
   �     �  O        S     j     �     �  S   �  +   �  %   "  ,   H     u     �     �     �     �  	   �     �     �     �               0     C     R  	   _     i  Y   {     �  $   �  E   	     O  �  e     �       a     %   |     �     �  	   �  X   �  .   8	  .   g	  3   �	     �	     �	     
     
     
  
   >
     I
     i
     u
     �
  !   �
     �
     �
  
   �
     �
       d        w  +   �  T   �                !                                                                                 
          	                                                         % comments <- Newer Posts <a href="%1$s" title="Visit %2$s's website" target="blank">Visit my website</a> <h2>404 NOT FOUND</h2> <h3>Content categories</h3> <h3>Tag Cloud</h3> <p>Page: <p>The article you were looking for was not found, but maybe try looking again!</p> <span class="author">Posted by %1$s</span>  <span class="category">in %1$s</span> <span class="date-published">at %1$s</span>  All rights reserved Daily Archives: %1$s Leave a comment Main Sidebar Monthly Archives: %1$s Next page Nothing post found. Older Posts -> One comment Posts by %1$s Posts categoried: %1$s Posts tagged: %1$s Previous page: Primary Menu Read More Read all comments Search result for <strong>%1$s</strong>. We found <strong>%2$s</strong> articles for you. Tagged in %1$s This image post contains %1$s photos This is widget area. Go to Appearance -> Widgets to add some widgets. Yearly Archives: %1$s Project-Id-Version: 
POT-Creation-Date: 2016-02-27 12:25+0700
PO-Revision-Date: 2016-02-27 12:46+0700
Last-Translator: 
Language-Team: 
Language: vi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7
X-Poedit-Basepath: ..
Plural-Forms: nplurals=1; plural=0;
X-Poedit-KeywordsList: __;_e
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 % bình luận <- Mới hơn <a href="%1$s" title="Đến website của %2$s" target="blank">Ghé thăm website của tôi</a> <h2>404 KHÔNG TÌM THẤY TRANG</h2> <h3>Danh mục bài viết</h3> <h3>Tag Cloud</h3> <p>Trang: <p>Bài viết bạn đang tìm kiếm hiện không có, hãy thử bài khác xem!</p> <span class="author">Đăng bởi %1$s</span>  <span class="category">trong chỗ %1$s</span> <span class="date-published">vào lúc %1$s</span>  Bản quyền thuộc về Lưu trữ hằng ngày: %1$s Bình luận Sidebar chính Lưu trữ hằng tháng: %1$s Trang kế Bài này không có gì hết. Cũ hơn -> Một bình luận Viết bởi %1$s Bài viết theo danh mục: %1$s Bài viết gắn tag: %1$s Trang trước: Menu Chủ Đọc tiếp Xem bình luận Kết quả cho <strong>%1$s</strong>. Đã tìm thấy <strong>%2$s</strong> bài viết cho bạn. Được tag trong %1$s Bài có hình này chứa %1$s tấm hình Đây là khu vực tiện ích. Vào Appearance -> Widgets để thêm tiện ích. Lưu trữ hằng năm: %1$s 