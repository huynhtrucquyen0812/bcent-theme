<?php
/**
  @ Thiết lập các hằng dữ liệu quan trọng
  @ THEME_URL = get_stylesheet_directory() - đường dẫn tới thư mục theme
  @ CORE = thư mục /core của theme, chứa các file nguồn quan trọng.
  **/
define('THEME_URL', get_stylesheet_directory());
define('CORE', THEME_URL . '/core');

/**
  @ Load file /core/init.php
  @ Đây là file cấu hình ban đầu của theme mà sẽ không nên được thay đổi sau này.
  **/
require_once(CORE . '/init.php');

/**
  @ Thiết lập $content_width để khai báo kích thước chiều rộng của nội dung
  **/
if(!isset($content_width)){
	$content_width = 620;
}

/**
  @ Thiết lập các chức năng sẽ được theme hỗ trợ
  **/
if(!function_exists('bcent_theme_setup')){
	function bcent_theme_setup(){
		/**
		  @ Thiết lập theme có thể dịch được
		  **/
		$language_folder = THEME_URL . '/languages';
		load_theme_textdomain('bcent', $language_folder);

		/**
		  @ Tự chèn RSS Feed links trong <head>
		  **/
		add_theme_support('automatic-feed-links');

		/**
		  @ Thêm chức năng post thumbnail
		  **/
		add_theme_support('post-thumbnails');

		/**
		  @ Thêm chức năng title-tag để tự thêm <title>
		  **/
		add_theme_support('title-tag');

		/**
		  @ Thêm chức năng post format
		  **/
		add_theme_support('post-formats',
			array('image', 'video', 'gallery', 'quote', 'link'));

		/**
		  @ Thêm chức năng custom background
		  **/
		$default_background = array('default-color'=>'#e6ddde');
		add_theme_support('custom-background', $default_background);

		/**
		  @ Tạo menu cho theme
		  **/
		register_nav_menu( 'primary-menu', __('Primary Menu', 'bcent') );

		/**
		  @ Tạo sidebar cho theme
		  **/
		$sidebar = array(
			'name' => __('Main Sidebar', 'bcent'),
			'id' => 'main-sidebar',
			'description' => 'Main sidebar for BCEnt theme',
			'class' => 'main-sidebar',
			'before_title' => '<h3 class="widgettitle">',
			'after_title' => '</h3>');
		register_sidebar($sidebar);
	}
	add_action('init', 'bcent_theme_setup');
}

/**
  @ Thiết lập hàm hiển thị logo
  **/
if(!function_exists('bcent_logo')){
	function bcent_logo(){ ?>
		<div class="logo">
			<div class="site-name">
				<?php
					global $bce_options;
					if ($bce_options['logo-on']==0) :
				?>
					<?php if(is_home()){
						printf(
							'<h1><a href="%1$s" title="%2$s">%3$s</a></h1>',
							get_bloginfo( 'url' ),
							get_bloginfo( 'description' ),
							get_bloginfo( 'sitename' )
						);
					} ?>
				<?php else: ?>
					<img src="<?php echo $bce_options['logo-image']['url'] ?>" style="max-height: 100px"/>
				<?php endif; ?>
			</div>
			<div class="site-description"><?php bloginfo( 'description' ); ?></div>
		</div>
		<?php
	}
}

/**
  @ Thiết lập hàm hiển thị menu
  @ thachpham_menu( $slug )
  **/
if(!function_exists('bcent_menu')){
	function bcent_menu($slug){
		$menu=array(
			'theme_location' => $slug,
			'container' => 'nav',
			'container_class' => $slug,
			'items_wrap' => '<ul id="%1$s" class="sf-menu">%3$s</ul>'
		);
		wp_nav_menu( $menu );
	}
}

/**
  @ Tạo hàm phân trang cho index, archive.
  @ Hàm này sẽ hiển thị liên kết phân trang theo dạng chữ: Newer Posts & Older Posts
  **/
if(!function_exists('bcent_pagination')){
	function bcent_pagination(){
		/* Không hiển thị phân trang nếu trang đó có ít hơn 2 trang */
		if($GLOBALS['wp_query']->max_num_pages < 2) return '';
	?>
	<nav class="pagination" role="navigation">
		<?php if(get_previous_posts_link()) : ?>
			<div class="next"><?php previous_posts_link(__('<- Newer Posts', 'bcent')); ?></div>
		<?php endif; ?>

		<?php if(get_next_posts_link()) : ?>
			<div class="prev"><?php next_posts_link(__('Older Posts ->', 'bcent')); ?></div>
		<?php endif; ?>
	</nav>
	
	<?php
	}
}

/**
  @ Hàm hiển thị ảnh thumbnail của post.
  @ Ảnh thumbnail sẽ không được hiển thị trong trang single
  @ Nhưng sẽ hiển thị trong single nếu post đó có format là Image
  **/
if(!function_exists('bcent_thumbnail')){
	function bcent_thumbnail($size){
		// Chỉ hiển thị thumbnail với post ko có mật khẩu
		if(!is_single() && has_post_thumbnail() && !post_password_required() || has_post_format( 'image' )) : ?>
		<figure class="post-thumbnail"><?php the_post_thumbnail($size); ?></figure>
		<?php endif;
	}
}

/**
  @ Hàm hiển thị tiêu đề của post trong .entry-header
  @ Tiêu đề của post sẽ là nằm trong thẻ <h1> ở trang single
  @ Còn ở trang chủ và trang lưu trữ, nó sẽ là thẻ <h2>
  **/
if(!function_exists('bcent_entry_header')){
	function bcent_entry_header(){
		if(is_single()) : ?>
			<h1 class="entry-title">
				<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
					<?php the_title(); ?>
				</a>
			</h1>
		<?php else : ?>
			<h2 class="entry-title">
				<a href="<?php the_permalink(); ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
					<?php the_title(); ?>
				</a>
			</h2>
		<?php endif;
	}
}

/**
  @ Hàm hiển thị thông tin của post (Post Meta)
  **/
if(!function_exists('bcent_entry_meta')){
	function bcent_entry_meta(){
		if(!is_page()) :
			echo '<div class="entry-meta">';

			// Hiển thị tên tác giả, tên category và ngày tháng đăng bài
        	printf(__('<span class="author">Posted by %1$s</span> ', 'bcent'), get_the_author());
        	if($trans=get_post_meta( get_the_id(), 'translator', true ))
	        	printf(__('<span class="translator">Translated by %1$s</span> ', 'bcent'), $trans);
        	printf(__('<span class="date-published">at %1$s</span> ', 'bcent'), get_the_date());
        	printf(__('<span class="category">in %1$s</span>', 'bcent'), get_the_category_list());
        	
        	// Hiển thị số đếm lượt bình luận
        	if(comments_open()) :
        		echo '<span class="meta-reply">';
        		comments_popup_link(
        			__('Leave a comment', 'bcent'),
        			__('One comment', 'bcent'),
        			__('% comments', 'bcent'),
        			__('Read all comments', 'bcent')
        		);
        		echo '</span>';
        	endif;

        	echo '</div>';
        endif;
	}
}

/**
  @ Thêm chữ Read More vào excerpt
  **/
function bcent_readmore(){
	return '...<a class="read-more" href="'.get_permalink(get_the_id()).'">'.__('Read More', 'bcent').'</a>';
}
add_filter('excerpt_more', 'bcent_readmore');

/**
  @ Hàm hiển thị nội dung của post type
  @ Hàm này sẽ hiển thị đoạn rút gọn của post ngoài trang chủ (the_excerpt)
  @ Nhưng nó sẽ hiển thị toàn bộ nội dung của post ở trang single (the_content)
  **/
if(!function_exists('bcent_entry_content')){
	function bcent_entry_content(){
		if(!is_single() && !is_page()) :
			the_excerpt();
		else :
			the_content();

			// Code hiển thị phân trang trong post type
	        $link_pages = array(
	        	'before' => __('<p>Page:', 'bcent'),
	        	'after' => '</p>',
	        	'nextpagelink' => __('Next page', 'bcent'),
	        	'previouspagelink' => __('Previous page:', 'bcent')
	        );
	        wp_link_pages( $link_pages );

        endif;
	}
}

/**
  @ Hàm hiển thị tag của post
  **/
if(!function_exists('bcent_entry_tag')){
	function bcent_entry_tag(){
		if(has_tag()) :
			echo '<div class="entry-tag">';
			printf(__('Tagged in %1$s', 'bcent'), get_the_tag_list('', ', '));
			echo '</div>';
		endif;
	}
}

/**
  @ Nhúng file style.css
  **/
function bcent_style(){
	wp_register_style( 'main-style', get_template_directory_uri() . '/style.css', 'all' );
	wp_enqueue_style( 'main-style' );
	wp_register_style( 'reset-style', get_template_directory_uri() . '/css/reset.css', 'all' );
	wp_enqueue_style( 'reset-style' );

	// Superfish Menu
	wp_register_style( 'superfish-style', get_template_directory_uri() . '/css/superfish.css', 'all' );
	wp_enqueue_style( 'superfish-style' );
	wp_register_script( 'superfish-script', get_template_directory_uri() . '/js/superfish.js', array('jquery'));
	wp_enqueue_script( 'superfish-script' );

	// Custom Script
	wp_register_script( 'custom-script', get_template_directory_uri() . '/js/custom.js', array('jquery'));
	wp_enqueue_script( 'custom-script' );
}
add_action( 'wp_enqueue_scripts', 'bcent_style' );

/**
  @ Filter Hook
  **/
function change_copyright($output){
	$output='Hello, footer changed.';
	return $output;
}
add_filter( 'bcent_copyright', 'change_copyright' );

function bcent_content_filter($content){
	$find='hello';
	$replacement='<strong>hello</strong>';
	$content=str_replace($find, $replacement, $content);
	return $content;
}
add_filter('the_content', 'bcent_content_filter');