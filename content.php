<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> >
	<div class="entry-thumbnail">
		<?php bcent_thumbnail('thumbnail'); ?>
	</div>
	<header class="entry-header">
		<?php bcent_entry_header(); ?>
		<?php bcent_entry_meta(); ?>
	</header>
	<div class="entry-content">
		<?php bcent_entry_content(); ?>
		<?php (is_single()?bcent_entry_tag() : ''); ?>
	</div>
</article>